const mm = require("micromatch")
const dnsbl = require("dnsbl")

async function register({
  registerHook,
  registerSetting,
  settingsManager
}) {
  registerSetting({
    name: "registration-allowed-email-right",
    label:
      "Enregistrement : saisir les domaines autorisés (à séparer par des virgules)",
    type: "input-textarea",
    private: true
  })

  registerHook({
    target: "filter:api.user.signup.allowed.result",
    handler: (result, params) =>
      filterRegistration(result, params, settingsManager)
  })
  
}

async function unregister() {
  return
}

module.exports = {
  register,
  unregister
}

async function filterRegistration(result, params, settingsManager) {
  // params: { body, ip }

  // check right part of the mail address for forbidden domains, if any
  const blockedEmailRight = (
    await settingsManager.getSetting("registration-allowed-email-right")
  )
    .replace(/\s/g, "")
    .split(",")
  if (!mm.isMatch(params.body.email.split("@")[1], blockedEmailRight)){
	return { allowed: false }
  }


  return result
}
