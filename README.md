# peertube-plugin-glavlit-custom

Peertube-plugin : autorisation de domaine pour l'inscription

Procédure d'installation du plugin pour l'autorisation de domaine pour l'inscription


1) copier les sources du plugin sur le serveur (PATH_PLUGIN)
2) lancer la commande : cd /var/www/peertube/peertube-latest/
3) lancer : NODE_CONFIG_DIR=/var/www/peertube/config NODE_ENV=production npm run plugin:install -- --plugin-path PATH_PLUGIN/peertube-plugin-glavlit-custom/
4) vérifier que l'installation s'est bien dérouler
5) relancer peertube avec : systemctl restart peertube
6) aller dans l'application dans la partie plugin, ce dernier devrait être installé, aller ensuite dans paramètre pour inscrire les domaines autorisés


Attention : le plugin s'ajoute au comportement normal de l'inscription, si pas de saisie, on laisse passer tout le monde.